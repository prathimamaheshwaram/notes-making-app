import 'date-fns';
import uuid from 'react-uuid'
import React, { useContext, useState } from 'react'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, FormHelperText, Input, InputLabel, TextareaAutosize, TextField, Typography } from '@material-ui/core'
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import { useForm, Controller } from "react-hook-form";
import { NotesContext } from '../context/NotesContext.jsx';

function AddNotes({ handleDialogueToggle, open, values }) {
    const { handleSubmit, control, errors } = useForm();
    const { addNote } = useContext(NotesContext)
    const onSubmit = data => {
        const id = values !== null ? values.id : uuid()
        addNote(data, id)
        handleDialogueToggle()
    };
    return (

        <MuiPickersUtilsProvider utils={DateFnsUtils}>

            <Dialog open={open} disableBackdropClick>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <DialogTitle>Add Notes</DialogTitle>
                    <DialogContent>
                        <FormControl fullWidth margin="dense">
                            <Controller
                                name="title"
                                defaultValue={values === null ? "" : values.title}
                                control={control}
                                rules={{ required: true }}
                                as={
                                    <TextField color="primary" label="Title"></TextField>
                                }
                            />
                            <FormHelperText error>{errors.title?.type === "required" && "Your input is required"}</FormHelperText>
                        </FormControl>
                        <FormControl fullWidth margin="dense" name="date" >
                            <Controller
                                control={control}
                                name="date"
                                defaultValue={values === null ? new Date() : values.date}
                                render={({ onChange, value }) => (
                                    <KeyboardDatePicker
                                        value={value}
                                        onChange={onChange}
                                        margin="normal"
                                        id="date-picker-dialog"
                                        label="Date picker dialog"
                                        format="dd/MM/yyyy"
                                    />
                                )}
                            />
                        </FormControl>

                        <FormControl fullWidth margin="dense">
                            <Typography variant="body2" gutterBottom >Add Description</Typography>
                            <Controller
                                name="description"
                                defaultValue={values === null ? "" : values.description}
                                control={control}
                                rules={{ required: true }}
                                as={
                                    <TextareaAutosize error={errors.description} id="description" rowsMin={6} />
                                }
                            />
                            <FormHelperText error>{errors.description?.type === "required" && "Your input is required"}</FormHelperText>
                        </FormControl>


                    </DialogContent>
                    <DialogActions>
                        <Button color="secondary" onClick={handleDialogueToggle}>Cancel</Button>
                        <Button color="primary" type="submit">Submit</Button>


                    </DialogActions>
                </form>
            </Dialog>

        </MuiPickersUtilsProvider >



    )
}

export default AddNotes
