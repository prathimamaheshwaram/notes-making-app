import { Button, Grid, InputAdornment, Paper, TextField, MenuItem } from '@material-ui/core'
import React, { useState } from 'react'
import SearchIcon from '@material-ui/icons/Search';


const months = [
    {
        value: 0,
        label: 'January',
    },
    {
        value: 1,
        label: 'February',
    },
    {
        value: 2,
        label: 'March',
    },
    {
        value: 3,
        label: 'April',
    },
    {
        value: 4,
        label: 'May',
    },
    {
        value: 5,
        label: 'June',
    },
    {
        value: 6,
        label: 'July',
    },
    {
        value: 7,
        label: 'August',
    },
    {
        value: 8,
        label: 'September',
    },
    {
        value: 9,
        label: 'October',
    },
    {
        value: 10,
        label: 'November',
    },
    {
        value: 11,
        label: 'December',
    },
];
const weeks = [
    {
        value: 0,
        label: 'Sunday',
    },
    {
        value: 1,
        label: 'Monday',
    },
    {
        value: 2,
        label: 'Tuesday',
    },
    {
        value: 3,
        label: 'Wednesday',
    },
    {
        value: 4,
        label: 'Thursday',
    },
    {
        value: 5,
        label: 'Friday',
    },
    {
        value: 6,
        label: 'Saturday',
    },
];
const years = [1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2021, 2022, 2023, 2024];
function Filters({ handleSearch, handleWeekFilter, handleMonthFilter, handleYearFilter }) {
    const [week, setWeek] = useState("all")
    const [month, setMonth] = useState("all")
    const [year, setYear] = useState("all")
    return (
        <Grid container alignItems="space-around" spacing={1}>
            <Grid item xs={3}>
                <TextField
                    label="search"
                    size="medium"
                    variant="filled"
                    helperText="enter name to search"
                    onChange={e => handleSearch(e.target.value.toLowerCase())}
                    InputProps={{
                        startAdornment: <InputAdornment position="start"><SearchIcon /></InputAdornment>,
                    }}
                />
            </Grid>
            <Grid item xs={3}>
                <TextField
                    value={week}
                    select
                    label="Weekwise Filter"
                    helperText="Please select a week"
                    variant="filled"
                    fullWidth
                    onChange={e => {
                        handleWeekFilter(e.target.value)
                        setWeek(e.target.value)
                        setMonth("all")
                        setYear("all")
                    }

                    }
                >
                    <MenuItem key="all" value="all">All</MenuItem>
                    {weeks.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem>
                    ))}
                </TextField>
            </Grid>
            <Grid item xs={3}>
                <TextField
                    size="medium"
                    value={month}
                    select
                    label="Month wise Filter"
                    helperText="Please select a month"
                    variant="filled"
                    fullWidth
                    onChange={e => {
                        handleMonthFilter(e.target.value)
                        setMonth(e.target.value)
                        setWeek("all")
                        setYear("all")
                    }

                    }
                >
                    <MenuItem key="all" value="all">All</MenuItem>
                    {months.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem>

                    ))}
                </TextField>
            </Grid>
            <Grid item xs={3}>

                <TextField
                    value={year}
                    select
                    label="year wise Filter"
                    helperText="Please select a year"
                    variant="filled"
                    fullWidth
                    onChange={e => {
                        handleYearFilter(e.target.value)
                        setYear(e.target.value)
                        setMonth("all")
                        setWeek("all")
                    }

                    }
                >
                    <MenuItem key="all" value="all">All</MenuItem>
                    {years.map((option, index) => (
                        <MenuItem key={index} value={option}>
                            {option}
                        </MenuItem>
                    ))}
                </TextField>
            </Grid>
        </Grid>

    )
}

export default Filters
