import { AppBar, Toolbar, Typography } from '@material-ui/core'
import React from 'react'
function NavBar() {
    return (
        <AppBar position="fixed">
            <Toolbar>
                <Typography variant="h5" color="inherit">Notes Making App</Typography>
            </Toolbar>
        </AppBar>
    )
}

export default NavBar
