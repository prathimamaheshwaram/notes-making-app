import React from 'react'
import NavBar from './NavBar'
import { Outlet } from 'react-router-dom'
import Footer from './Footer'
import NotesContextProvider from '../context/NotesContext'

function Dashboard() {
    return (
        <>
            <NavBar />
            <NotesContextProvider>
                <Outlet />
            </NotesContextProvider>
            <Footer />
        </>
    )
}

export default Dashboard