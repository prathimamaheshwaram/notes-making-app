import React, { useState, useContext } from 'react'
import { useStyles } from './styles.jsx'
import NotesList from './NotesList.jsx'
import { List, Paper, ListSubheader, Grid, Toolbar, Typography, ListItem, ListItemText, Avatar, IconButton, ListItemSecondaryAction, Tooltip, Fab, ListItemIcon, TextField, InputAdornment } from '@material-ui/core';
import { NotesContext } from '../context/NotesContext.jsx';
import Filters from './Filters.jsx';
import AddNotes from './AddNotes.jsx';
import FilterListIcon from '@material-ui/icons/FilterList';
import AddIcon from '@material-ui/icons/Add';

function DisplayNotes() {
    const { notes, removeNote } = useContext(NotesContext)
    const [sortedNotes, setSortedNotes] = useState(notes)
    const [isNewFirst, sortIsNewFirst] = useState(true)
    const classes = useStyles();
    const [open, setOpen] = useState([]);
    const [openDialogue, setOpenDialogue] = useState(false);
    const handleDialogueToggle = () => {
        setOpenDialogue(!openDialogue)
        console.log(openDialogue);
    }
    const handleClick = id => {
        const close = open.includes(id)
        if (close) {
            const openedList = open.filter(e => { return e !== id });
            setOpen([...openedList])
        }
        else setOpen([...open, id])
    }
    const handleSort = () => {
        let sorted = isNewFirst ? sortedNotes.sort((a, b) => { return b.date - a.date }) : sortedNotes.sort((a, b) => { return a.date - b.date });
        sortIsNewFirst(!isNewFirst)
        setSortedNotes(sorted)

    }
    const handleSearch = (str) => {
        let results = notes.filter((note) => { return note.title.includes(str) })
        setSortedNotes(results)
        console.log(results);
    }
    const handleWeekFilter = (week) => {
        const weekly = week === "all" ? notes : notes.filter(note => { return note.date.getDay() === week })
        setSortedNotes(weekly)
    }
    const handleMonthFilter = (month) => {
        const monthly = month === "all" ? notes : notes.filter(note => { return note.date.getMonth() === month })
        setSortedNotes(monthly)
    }
    const handleYearFilter = (year) => {
        const yearly = year === "all" ? notes : notes.filter(note => { return note.date.getFullYear() === year })
        setSortedNotes(yearly)
    }
    return (
        <Grid container className={classes.root} justify="center" alignItems="center" spacing={6} >
            <Grid item xs={12}></Grid>
            <Grid item xs={12}></Grid>
            <Grid item xs={8}>
                <Filters handleSearch={handleSearch} handleWeekFilter={handleWeekFilter} handleMonthFilter={handleMonthFilter} handleYearFilter={handleYearFilter} />


                {open && <AddNotes handleDialogueToggle={handleDialogueToggle} values={null} open={openDialogue} />}
            </Grid>
            <Grid item xs={8}>
                <List
                    component={Paper}
                    aria-labelledby="nested-list-subheader"
                    subheader={
                        <ListSubheader component="div" id="nested-list-subheader">
                            <Toolbar >

                                <ListItemIcon>
                                    <Tooltip title="sort based on date" aria-label="sort">
                                        <Fab color="primary" size="small" onClick={handleSort}>
                                            <FilterListIcon />
                                        </Fab>
                                    </Tooltip>
                                </ListItemIcon>

                                <ListItemSecondaryAction>
                                    <Tooltip title="Add notes" aria-label="sort">
                                        <Fab color="primary" size="small" onClick={handleDialogueToggle}><AddIcon /></Fab>
                                    </Tooltip>
                                </ListItemSecondaryAction>
                            </Toolbar>
                        </ListSubheader>
                    }
                    className={classes.root}
                >
                    {
                        sortedNotes.length !== 0 ? sortedNotes.map(note => {
                            return <NotesList
                                note={note}
                                key={note.id}
                                handleClick={handleClick}
                                open={open}
                                handleDelete={removeNote}
                            />
                        }
                        )
                            : <ListItem><ListItemText secondary="no notes to show" /></ListItem>
                    }
                </List>

            </Grid>
        </Grid>
    )
}

export default DisplayNotes
