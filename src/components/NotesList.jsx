import React, { useState } from 'react'
import { Collapse, IconButton, List, ListItem, ListItemSecondaryAction, ListItemIcon, ListItemText } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddNotes from './AddNotes';


function NotesList({ note, handleClick, open, handleDelete }) {
    const { title, description, date, id } = note
    const [openDialogue, setOpenDialogue] = useState(false)
    const handleDialogueToggle = () => setOpenDialogue(!openDialogue)
    return (
        <>
            <ListItem button onClick={e => handleClick(id)} >
                <ListItemIcon >
                    <IconButton >
                        <ExpandMore />
                    </IconButton>
                </ListItemIcon>

                <ListItemText primary={title} secondary={note.date.toDateString()} />
                <ListItemSecondaryAction>
                    <IconButton onClick={e => handleDelete(id)} >
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
            <Collapse in={open.includes(id)} timeout="auto" unmountOnExit>
                <List component="div">
                    {openDialogue && <AddNotes open={openDialogue} values={note} handleDialogueToggle={handleDialogueToggle} />}
                    <ListItem button onClick={handleDialogueToggle}>
                        <ListItemIcon >
                            <IconButton>
                                <EditIcon />
                            </IconButton>
                        </ListItemIcon>
                        <ListItemText primary={description} />
                    </ListItem>
                </List>
            </Collapse>
        </>
    )
}

export default NotesList
