import Dashboard from "./components/Dashboard";
import { useRoutes } from 'react-router-dom'
import DisplayNotes from "./components/DisplayNotes";


function App() {
  let routes = useRoutes([
    {
      path: '/',
      element: <Dashboard />,
      children: [
        { path: '/', element: <DisplayNotes /> },
        // {path : 'edit',element:},
      ]
    }
  ])
  return (
    <>
      {routes}
    </>
  );
}

export default App;
