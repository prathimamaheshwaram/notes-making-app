import React, { createContext, useState } from 'react'
export const NotesContext = createContext();
const note = [
    {
        title: "one",
        description: "Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. ",
        date: new Date(2018, 11, 15),
        id: 1
    },
    {
        title: "two",
        description: "Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. ",
        date: new Date(2017, 12, 6),
        id: 2
    },
    {
        title: "three",
        description: "Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. ",
        date: new Date(2017, 1, 6),
        id: 3
    },
    {
        title: "four",
        description: "Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. ",
        date: new Date(2017, 12, 4),
        id: 4
    },
    {
        title: "five",
        description: "Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. ",
        date: new Date(2014, 12, 25),
        id: 5
    },
    {
        title: "six",
        description: "Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. ",
        date: new Date(2019, 2, 16),
        id: 6
    },
    {
        title: "seven",
        description: "Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. ",
        date: new Date(2021, 5, 30),
        id: 7
    },


]
const NotesContextProvider = (props) => {
    const [notes, setNote] = useState(note)
    const addNote = ({ title, description, date }, id) => {
        const newNotes = notes.filter(note => { return note.id !== id })
        setNote([...newNotes, { title, description, date, id }]);
    }
    const removeNote = (id) => {
        const newList = notes.filter(ele => ele.id !== id)
        setNote([...newList])
    }
    return (
        <NotesContext.Provider value={{ notes, addNote, removeNote }}>
            {props.children}
        </NotesContext.Provider>
    );
}

export default NotesContextProvider;
